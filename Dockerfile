# Alpine Mono image
FROM frolvlad/alpine-mono

MAINTAINER Isaac A., <isaac@isaacs.site>

RUN apk add --no-cache --update curl ca-certificates openssl git tar bash sqlite fontconfig wget

# NodeJS
RUN apk add nodejs nodejs-npm

# PHP 7
RUN apk add php7 php7-fpm php7-mcrypt php7-soap php7-openssl php7-gmp php7-pdo_odbc php7-json php7-dom php7-pdo php7-zip php7-mysqli php7-sqlite3 php7-apcu php7-pdo_pgsql php7-bcmath php7-gd php7-odbc php7-pdo_mysql php7-pdo_sqlite php7-gettext php7-xmlreader php7-xmlrpc php7-bz2 php7-iconv php7-pdo_dblib php7-curl php7-ctype php7-phar php7-fileinfo php7-mbstring php7-tokenizer

# Java
RUN apk add openjdk8

# Python
RUN apk add python2 py2-pip python3

# Lua
RUN apk add lua5.3

RUN adduser -D -h /home/container container

USER container
ENV  USER=container HOME=/home/container

WORKDIR /home/container

COPY ./entrypoint.sh /entrypoint.sh

CMD ["/bin/bash", "/entrypoint.sh"]